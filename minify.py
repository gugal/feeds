#!/usr/bin/env python3
"""
JSON minify program.
Adapted from https://gist.github.com/KinoAR/a5cf8a207529ee643389c4462ebf13cd.
"""

import json

file_data = open("feeds.json", "r", 1).read() # store file info in variable
json_data = json.loads(file_data) # store in json structure
json_string = json.dumps(json_data, separators=(',', ":")) # Compact JSON structure
open("feeds_min.json", "w", 1).write(json_string) # open and write json_string to file